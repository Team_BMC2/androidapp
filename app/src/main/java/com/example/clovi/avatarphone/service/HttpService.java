package com.example.clovi.avatarphone.service;




import com.example.clovi.avatarphone.domain.User;
import com.example.clovi.avatarphone.routes.Routes;

import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;

/**
 * Created by clovi on 25/02/2017.
 */

public class HttpService{
    private Retrofit retrofit;
    private Routes routes;
    private User user;

    private static HttpService instance;

    public HttpService() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://cours.mrzee.fr/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        routes = retrofit.create(Routes.class);
    }


    public Promise<List<User>, Throwable, Void> getUsers(){
        final Deferred<List<User>, Throwable, Void> deferred = new DeferredObject<>();
        routes.getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                deferred.resolve(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                deferred.reject(t);
            }
        });
        return deferred.promise();
    }
    public Promise<User, Throwable, Void> createUser(User user){
        final Deferred<User, Throwable, Void> deferred = new DeferredObject<>();
        routes.setUsers(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                deferred.resolve(response.body());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                deferred.reject(t);

            }
        });
        return deferred.promise();
    }

    public Promise<User,Throwable,Void> authenticateUser(User user){
        final Deferred<User, Throwable, Void> deferred = new DeferredObject<>();
        routes.authenticate(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                deferred.resolve(response.body());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                deferred.reject(t);

            }
        });
        return deferred.promise();
    }

    //permet de faire le token
    public static HttpService getInstance(){
        if(instance == null){
            instance = new HttpService();
        }
        return instance;
    }
}
