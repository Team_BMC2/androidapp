package com.example.clovi.avatarphone.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by clovi on 25/02/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

        private String username;

        public String getUsername() {

            return username;
        }

        public void setUsername(String username) {

            this.username = username;
        }


    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
