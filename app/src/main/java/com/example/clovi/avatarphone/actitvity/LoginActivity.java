package com.example.clovi.avatarphone.actitvity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.domain.User;
import com.example.clovi.avatarphone.service.HttpService;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    Button bLogin;
    EditText etUsername, etPassword;
    TextView registerLink;
    HttpService httpService = HttpService.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        correspondanceLogin();

    }

    public void correspondanceLogin(){
        bLogin = (Button) findViewById(R.id.bLogin);
        etUsername = (EditText) findViewById(R.id.etUsername);
        bLogin.setOnClickListener(this);
        etPassword = (EditText) findViewById(R.id.etPassword);
        registerLink.setOnClickListener(this);
        registerLink = (TextView) findViewById(R.id.tvRegisterLink);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bLogin :

                    String password = etPassword.getText().toString();
                    String username = etUsername.getText().toString();

                    User userLogin = new User();
                    userLogin.setUsername(username);
                    userLogin.setPassword(password);
                    httpService.authenticateUser(userLogin).done(new DoneCallback<User>() {
                        @Override
                        public void onDone(User result) {
                            startActivity(new Intent(LoginActivity.this, AccueilActivity.class));
                            finish();
                        }
                    }).fail(new FailCallback<Throwable>() {
                        @Override
                        public void onFail(Throwable result) {
                            Toast.makeText(LoginActivity.this, "erreur", Toast.LENGTH_SHORT).show();
                        }
                    });



                break;
            case R.id.tvRegisterLink :
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }
}
