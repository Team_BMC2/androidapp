package com.example.clovi.avatarphone.domain;

/**
 * Created by clovi on 07/03/2017.
 */

public class Avatar {

    public String username;
    public int size;
    public int border;

    public Avatar(String username, int size, int border){
        this.username = username;
        this.size = size;
        this.border = border;
    }
}
