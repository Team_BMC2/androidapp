package com.example.clovi.avatarphone.test;

/**
 * Created by clovi on 28/02/2017.
 */

public class ClassAvecGeneric<T> {

    private T maVariable;

    public ClassAvecGeneric(T maVariable) {
        this.maVariable = maVariable;
    }

    public T getMaVariable() {
        return maVariable;
    }
}
