package com.example.clovi.avatarphone.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.clovi.avatarphone.fragment.ActiviteFragment;
import com.example.clovi.avatarphone.fragment.HumeurFragment;

import java.util.ArrayList;

/**
 * Created by clovi on 06/03/2017.
 */

public class Swipe extends FragmentPagerAdapter {
    public String fragments [] = {"fragment 1", "fragment 2"};

    public Swipe(FragmentManager fm, Context ctx) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new HumeurFragment();
            case 1:
                return new ActiviteFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    /*@Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }*/

}
