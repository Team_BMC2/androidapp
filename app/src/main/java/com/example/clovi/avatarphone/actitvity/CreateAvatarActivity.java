package com.example.clovi.avatarphone.actitvity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.domain.Avatar;
import com.example.clovi.avatarphone.service.HttpServiceAvatar;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;

import java.util.List;

/**
 * Created by clovi on 07/03/2017.
 */

public class CreateAvatarActivity extends Activity implements View.OnClickListener{
    private EditText etUsername;
    private EditText etSize;
    private EditText etBorder;
    private Button bCGood;
    private Button bVerifier;
    private HttpServiceAvatar httpServiceAvatar =new HttpServiceAvatar();
    private ImageView imageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar_create);
        correspodance();
        bVerifier.setOnClickListener(this);
    }





    public void correspodance (){
        etUsername = (EditText) findViewById(R.id.etUsername);
        etSize = (EditText) findViewById(R.id.etSize);
        etBorder = (EditText) findViewById(R.id.etBorder);
        bCGood = (Button) findViewById(R.id.bCGood);

        bVerifier = (Button) findViewById(R.id.bVerifier);
        imageView = (ImageView) findViewById(R.id.ivAvatar);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bCGood:

                break;

            case R.id.bVerifier:
                String username;
                int size;
                int border = 15;
                username = etUsername.getText().toString();
                size = Integer.parseInt(etSize.getText().toString());
               // border = Integer.parseInt(etBorder.getText().toString());
                final Avatar avatar = new Avatar(username,size,border);
                httpServiceAvatar.getAvatar(avatar).done(new DoneCallback<byte[]>() {
                    @Override
                    public void onDone(byte[] result) {
                        Bitmap bmp= BitmapFactory.decodeByteArray(result,0,result.length);
                        imageView =new ImageView(CreateAvatarActivity.this);
                        imageView.setImageBitmap(bmp);
                       // imageView.setImageBitmap();
                    }
                }).fail(new FailCallback<Throwable>() {
                    @Override
                    public void onFail(Throwable result) {
                        result.printStackTrace();

                    }
                });

                break;


        }
    }
}
