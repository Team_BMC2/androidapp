package com.example.clovi.avatarphone.fragment;


import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.clovi.avatarphone.actitvity.AccueilActivity;
import com.example.clovi.avatarphone.R;

import java.util.zip.Inflater;

/**
 * Created by clovi on 21/02/2017.
 */

public class HumeurFragment extends Fragment {
    private View vueHumeur;
    private Spinner spinnerHumeur;
    private ArrayAdapter<CharSequence> arrayAdapter;
    private ArrayAdapter<String> arrayAdapterList;
    private ListView listViewPosNeg;
    public String typeHumeurSelected;
    public int typeHumeurSelectedId;
    public int humeurSelectedId;
    public Button bValider;
    public AccueilActivity accueilActivity;
    public Image imageSelected;
    Drawable drawable;

    public ImageView smileys;
    public ImageView imageSauv;
    public Intent intent;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        vueHumeur = inflater.inflate(R.layout.fragment_humeur, container, false);
        bValider = (Button) vueHumeur.findViewById(R.id.bValider);
        clickValider();

        appelSpinner();

        return vueHumeur;
    }

    private void clickValider(){
        bValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
    }

    public static HumeurFragment newInstance(){
        HumeurFragment humeurFragment = new HumeurFragment();
        return humeurFragment;
    }

    public void appelSpinner(){
        spinnerHumeur = (Spinner) vueHumeur.findViewById(R.id.spinner_humeur);
        arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.humeur_type_names, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHumeur.setAdapter(arrayAdapter);
        spinnerHumeur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeHumeurSelected = (String) parent.getItemAtPosition(position);
               // Toast.makeText(getActivity(),parent.getItemIdAtPosition(position)+" Sélectionné", Toast.LENGTH_SHORT).show();
                appelListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void appelListView(){
        String[] listPositif = {"Joyeux", "Amoureux","Confiant","Aux anges","Rassuré","Etonné"};
        String[] listNegatif = {"Abbatu", "Affamé","Angoissé","Dégouté","Triste","Faché"};
        if (typeHumeurSelected.equals("Positif")){
            listViewPosNeg = (ListView) vueHumeur.findViewById(R.id.id_listView_humeur);
            arrayAdapterList = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listPositif);
            listViewPosNeg.setAdapter(arrayAdapterList);


        }
        else {
            listViewPosNeg = (ListView) vueHumeur.findViewById(R.id.id_listView_humeur);
            arrayAdapterList = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listNegatif);
            listViewPosNeg.setAdapter(arrayAdapterList);

        }

        listViewPosNeg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                humeurSelectedId = (int) parent.getItemIdAtPosition(position);
                Toast.makeText(getActivity(),+humeurSelectedId +" Sélectionné", Toast.LENGTH_LONG).show();
                appelSmileys();




            }
        });




    }

    public void appelSmileys(){
        smileys = (ImageView) vueHumeur.findViewById(R.id.smileys);
        int [] tabSmileysPos = {R.drawable.joyeux, R.drawable.amoureux,R.drawable.confiant, R.drawable.aux_anges, R.drawable.rassure, R.drawable.etonne};
        int [] tabSmileysNeg = {R.drawable.abattu, R.drawable.affame, R.drawable.angoisse, R.drawable.degouter, R.drawable.triste, R.drawable.fache};
        if (typeHumeurSelected.equals("Positif")){
            smileys.setImageResource(tabSmileysPos[humeurSelectedId]);
            intent = new Intent(getActivity(), AccueilActivity.class);
            intent.putExtra("imagePathHumeur",tabSmileysPos[humeurSelectedId] );
        }else{
            smileys.setImageResource(tabSmileysNeg[humeurSelectedId]);
            intent = new Intent(getActivity(), AccueilActivity.class);
            intent.putExtra("imagePathHumeur",tabSmileysPos[humeurSelectedId] );
        }





    }




}
