package com.example.clovi.avatarphone.actitvity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TableLayout;

import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.adapter.Swipe;
import com.example.clovi.avatarphone.adapter.SwipeFragment;
import com.example.clovi.avatarphone.fragment.ActiviteFragment;
import com.example.clovi.avatarphone.fragment.HumeurFragment;

public class DeuxiemePage extends AppCompatActivity {
    Swipe swipe;
    TabLayout tab;
    ViewPager vp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deuxieme_page);

        vp =(ViewPager) findViewById(R.id.view_pager);


        tab = (TabLayout) findViewById(R.id.tabs);
        tab.setupWithViewPager(vp);
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }
        });
        appelImage();

    }





    public void appelImage(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(new Swipe(getSupportFragmentManager(), getApplicationContext()));
    }



}
