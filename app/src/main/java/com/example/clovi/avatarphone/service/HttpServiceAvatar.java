package com.example.clovi.avatarphone.service;

import com.example.clovi.avatarphone.domain.Avatar;
import com.example.clovi.avatarphone.domain.User;
import com.example.clovi.avatarphone.routes.Routes;

import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by clovi on 07/03/2017.
 */

public class HttpServiceAvatar {
    private Retrofit retrofit;
    private Routes routes;


    public HttpServiceAvatar() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://avatars.adorable.io/#demo")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        routes = retrofit.create(Routes.class);
    }

    public Promise<byte[], Throwable, Void> getAvatar(Avatar avatar){
        final Deferred<byte[], Throwable, Void> deferred = new DeferredObject<>();
        routes.getAvatars(avatar.size, avatar.username).enqueue(new Callback<byte[]>() {
            @Override
            public void onResponse(Call<byte[]> call, Response<byte[]> response) {
                deferred.resolve(response.body());

            }

            @Override
            public void onFailure(Call<byte[]> call, Throwable t) {
                deferred.reject(t);

            }
        });
        return deferred.promise();
    }

   /* public Promise<Avatar, Throwable, Void> createAvatar(Avatar avatar){
        final Deferred<Avatar, Throwable, Void> deferred = new DeferredObject<>();
        routes.setAvatar(avatar).enqueue(new Callback<Avatar>() {
            @Override
            public void onResponse(Call<Avatar> call, Response<Avatar> response) {
                deferred.resolve(response.body());

            }

            @Override
            public void onFailure(Call<Avatar> call, Throwable t) {
                deferred.reject(t);

            }
        });
        return deferred.promise();
    }*/
}
