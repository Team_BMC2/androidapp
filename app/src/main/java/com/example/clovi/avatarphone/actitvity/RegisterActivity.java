package com.example.clovi.avatarphone.actitvity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.domain.User;
import com.example.clovi.avatarphone.service.HttpService;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    Button bRegister;
    EditText etName, etEmail, etUsername, etPassword;
    private HttpService httpService = HttpService.getInstance();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        correspondanceRegister();
        bRegister.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bRegister :

                    String username = etUsername.getText().toString();
                    String email = etEmail.getText().toString();
                    String password = etPassword.getText().toString();
                    User usercreated = new User();
                    usercreated.setUsername(username);
                    usercreated.setEmail(email);
                    usercreated.setPassword(password);
                    Toast.makeText(this, "L'utilisateur " + username+ " a été crée",Toast.LENGTH_SHORT).show();
                    httpService.createUser(usercreated).done(new DoneCallback<User>() {
                        @Override
                        public void onDone(User result) {
                            retourLogin();
                        }
                    }).fail(new FailCallback<Throwable>() {
                        @Override
                        public void onFail(Throwable result) {
                            result.printStackTrace();
                        }
                    });

                break;


        }
    }
    public void correspondanceRegister(){
        bRegister = (Button) findViewById(R.id.bRegister);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void retourLogin(){
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
    }

}
