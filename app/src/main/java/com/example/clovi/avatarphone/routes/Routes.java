package com.example.clovi.avatarphone.routes;

import com.example.clovi.avatarphone.domain.Avatar;
import com.example.clovi.avatarphone.domain.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by clovi on 25/02/2017.
 */

public interface Routes {
    @GET("/user/all")
    public Call<List<User>> getUsers();
    @POST("/user")
    public Call<User> setUsers(@Body User createdUser);
    @POST("/login")
    public Call<User> authenticate(@Body  User userAuthenticate);
    @GET("https://api.adorable.io/avatars/{size}/{username}")
    public Call<byte[]> getAvatars(@Path("size") int pSize, @Path("username") String pUsername);
  /*  @POST
    public Call<Avatar> setAvatar(@Body Avatar createAvatar);*/
}
