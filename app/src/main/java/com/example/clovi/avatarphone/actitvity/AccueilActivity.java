package com.example.clovi.avatarphone.actitvity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.adapter.SwipeFragment;
import com.example.clovi.avatarphone.fragment.ActiviteFragment;
import com.example.clovi.avatarphone.fragment.HumeurFragment;

/**
 * Created by clovi on 21/02/2017.
 */

public class AccueilActivity extends AppCompatActivity implements View.OnClickListener{
    private FrameLayout containerHumeur;
    ImageView imageView;
    ImageButton caseHumeur;
    ImageButton caseActivite;
    Button bLogout;
    ViewPager viewPager;
    TextView createAvatar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        correspondanceAccueil();
        bLogout.setOnClickListener(this);
        caseActivite.setOnClickListener(this);
        caseHumeur.setOnClickListener(this);
        createAvatar.setOnClickListener(this);
      //  changeAvatar();



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int iSelectedItemHumeur = bundle.getInt("imagePathHumeur");
            int iSelectedItemActivite = bundle.getInt("imagePathActivite");
             caseActivite.setImageResource(iSelectedItemActivite);
             caseHumeur.setImageResource(iSelectedItemHumeur);
             Toast.makeText(this, "salut", Toast.LENGTH_SHORT).show();
        }

       // addHumeur();
    }


    public void addHumeur(){

        FragmentManager manager = getFragmentManager();
        FragmentTransaction tx = manager.beginTransaction();
        //tx.replace(R.id.id_fr_humeur, new HumeurFragment());
        //tx.replace(R.id.id_fr_humeur, new ActiviteFragment());
        tx.commit();

    }

    public void correspondanceAccueil(){
        bLogout = (Button) findViewById(R.id.bLogout);

        caseHumeur = (ImageButton) findViewById(R.id.id_caseHumeur);
        caseActivite = (ImageButton) findViewById(R.id.id_caseActivite);
        //viewPager = (ViewPager) findViewById(R.id.vpAvatar);
        createAvatar = (TextView) findViewById(R.id.createAvatar);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bLogout:
                        startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.id_caseActivite:
                        startActivity(new Intent(this, DeuxiemePage.class));
                         finish();
                break;
            case R.id.id_caseHumeur:
                        startActivity(new Intent(this, DeuxiemePage.class));
                         finish();
                break;
            case R.id.createAvatar :
                        startActivity(new Intent(this, CreateAvatarActivity.class));
                break;
        }
    }

    public void changeAvatar(){
        SwipeFragment swipeFragment;
        swipeFragment = new SwipeFragment(this);
        viewPager.setAdapter(swipeFragment);
    }
}
