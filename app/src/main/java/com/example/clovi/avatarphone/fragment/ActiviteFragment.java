package com.example.clovi.avatarphone.fragment;



import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.clovi.avatarphone.R;
import com.example.clovi.avatarphone.actitvity.AccueilActivity;

/**
 * Created by clovi on 27/02/2017.
 */

public class ActiviteFragment extends Fragment{
    private Spinner spinnerActivite;
    private ArrayAdapter<CharSequence> arrayAdapter;
    private ArrayAdapter<String> arrayAdapterList;
    private ListView listViewActivite;
    public String typeActiviteSelected;
    public int typeActiviteSelectedId;
    public int activiteSelectedId;
    public ImageView photo;
    public Intent intent;
    public Button bValider;

    private View vueActivite;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vueActivite = inflater.inflate(R.layout.fragment_activite, container, false);
        bValider = (Button) vueActivite.findViewById(R.id.bValider);
        clickValider();
        appelSpinner();

        return vueActivite;
    }

    public static ActiviteFragment newInstance(){
        ActiviteFragment activiteFragment = new ActiviteFragment();
        return activiteFragment;
    }

    private void clickValider(){
        bValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);

            }
        });
    }

    public void appelSpinner() {
        spinnerActivite = (Spinner) vueActivite.findViewById(R.id.spinner_activite);
        arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.activite_type_names, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerActivite.setAdapter(arrayAdapter);
        spinnerActivite.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeActiviteSelected = (String) parent.getItemAtPosition(position);
                // Toast.makeText(getActivity(),parent.getItemIdAtPosition(position)+" Sélectionné", Toast.LENGTH_SHORT).show();
                appelListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void appelListView() {
        String[] listCalme = {"je dors", "je me reveille", "je regarde la TV"};
        String[] listNeutre = {"je suis malade", "je suis fatigué(é)", "je suis au toilette zer"};
        String[] listAgite = {"je suis en boite", "je fais du sport", "je suis de sorti"};

        if (typeActiviteSelected.equals("Calme")) {
            listViewActivite = (ListView) vueActivite.findViewById(R.id.id_listView_activite);
            arrayAdapterList = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listCalme);
            listViewActivite.setAdapter(arrayAdapterList);


        } else if (typeActiviteSelected.equals("Neutre")) {
            listViewActivite = (ListView) vueActivite.findViewById(R.id.id_listView_activite);
            arrayAdapterList = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listNeutre);
            listViewActivite.setAdapter(arrayAdapterList);

        } else {
            listViewActivite = (ListView) vueActivite.findViewById(R.id.id_listView_activite);
            arrayAdapterList = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listAgite);
            listViewActivite.setAdapter(arrayAdapterList);
        }

        listViewActivite.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                activiteSelectedId = (int) parent.getItemIdAtPosition(position);
                Toast.makeText(getActivity(), +activiteSelectedId + " Sélectionné", Toast.LENGTH_LONG).show();
                appelPhotos();
            }
        });

    }

    public void appelPhotos() {
        photo = (ImageView) vueActivite.findViewById(R.id.smileys);
        int[] tabPhotoCalme = {R.drawable.dormir, R.drawable.se_reveiller, R.drawable.regarder_tv};
        int[] tabPhotoNeutre = {R.drawable.malade, R.drawable.fatigue, R.drawable.toilette};
        int[] tabPhotoAgite = {R.drawable.boite, R.drawable.sport, R.drawable.de_sorti};

        if (typeActiviteSelected.equals("Calme")) {
            photo.setImageResource(tabPhotoCalme[activiteSelectedId]);
            intent = new Intent(getActivity(), AccueilActivity.class);
            intent.putExtra("imagePathActivite",tabPhotoCalme[activiteSelectedId] );
        } else if (typeActiviteSelected.equals("Neutre")) {
            photo.setImageResource(tabPhotoNeutre[activiteSelectedId]);
            intent = new Intent(getActivity(), AccueilActivity.class);
            intent.putExtra("imagePathActivite",tabPhotoNeutre[activiteSelectedId] );
        } else {
            photo.setImageResource(tabPhotoAgite[activiteSelectedId]);
            intent = new Intent(getActivity(), AccueilActivity.class);
            intent.putExtra("imagePathActivite",tabPhotoAgite[activiteSelectedId] );
        }
    }
}
