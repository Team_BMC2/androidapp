package com.example.clovi.avatarphone.test;

/**
 * Created by clovi on 28/02/2017.
 */

public class ClasseAvecBooleen {

    private Boolean maVariable;

    public ClasseAvecBooleen(Boolean maVariable) {
        this.maVariable = maVariable;
    }

    public Boolean getMaVariable() {
        return maVariable;
    }
}
