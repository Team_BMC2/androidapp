package com.example.clovi.avatarphone.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clovi.avatarphone.R;

/**
 * Created by clovi on 06/03/2017.
 */

public class SwipeFragment extends PagerAdapter {
    private int[] tab_fragment={R.drawable.av1, R.drawable.av2, R.drawable.av3,R.drawable.av4};
    private LayoutInflater layoutInflater;
    private Context context;

    public SwipeFragment(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return tab_fragment.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //mettre en place me contexte
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //donner une vue a l'adapter
        View item_view = layoutInflater.inflate(R.layout.fragment_swipe, container, false);
        //relier les images avec les "vues images"
        ImageView imageView = (ImageView) item_view.findViewById(R.id.image_view);
        TextView textView =(TextView) item_view.findViewById(R.id.image_count);
        //inserer l'image
        imageView.setImageResource(tab_fragment[position]);
        //indiquer le contenu de image_count
        textView.setText("image :"+position);
        //ajouter la vue crée dans un container
        container.addView(item_view);
        //retourner la vue
        return item_view;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
